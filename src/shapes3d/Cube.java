package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    private double height ;

    public Cube() {
        super();
        this.height =getEdge();
    }

    public Cube(double height) {
        super();
        this.height = height;
    }
    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    public double getVolume(){
        return height*height*height;
    }
    public double getAreaC(){
        return height*height*6;
    }

    @Override
    public String toString() {
        return "Cube{" +
                "height=" + height +
                '}';
    }
}
