package shapes2d;

public class Square {
    private  double edge;
    private  String color;
    public Square(){
        this.edge= 5.0;
        this.color="blue";
    }
    public Square(double edge){
        this.edge=edge;
        this.color="red";
    }
    public Square(double edge, String color){
        this.edge=edge;
        this.color=color;
    }

    public double getEdge() {
        return edge;
    }

    public String getColor() {
        return color;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Square{" +
                "edge=" + edge +
                ", color='" + color + '\'' +
                '}';
    }
    public double getArea(){
        return edge*edge;
    }
}
