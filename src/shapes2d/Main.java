package shapes2d;

import shapes3d.Cube;
import shapes3d.Cylinder;

public class Main {

    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(10, 20, "red");
        System.out.println("Radius is " + c1.getRadius()
                + ", Height is " + c1.getHeight()
                + ", Color is " + c1.getColor()
                + ", Cylinder area is " + c1.getAreaC()
                + ", Volume is " + c1.getVolume());
        Cube c2 = new Cube(10);
        System.out.println("Edge length  is " + c2.getHeight()
                + ", Height is " + c2.getHeight()
                + ", Color is " + c2.getColor()
                + ", Cube area is " + c2.getAreaC()
                + ", Volume is " + c2.getVolume());

    }
}
